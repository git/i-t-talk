all: s5-checksums.txt

clean:
	rm -rf s5-checksums.txt s5-blank *.dia.svg *.xcf.png

s5-checksums.txt: /usr/share/s5/s5-blank
	s5 blank .
	sed -i 's/url(bodybg.gif)//; s/\btext-transform: capitalize\s*;//' \
		s5-blank/ui/default/pretty.css

%.dia.svg: %.dia
	dia --export=$@ $<

%.xcf.png: %.xcf
	gimp -i -d -b '							'\
'(let* ((image (car (gimp-file-load RUN-NONINTERACTIVE "$<" "$<")))	'\
'       (layer (car (gimp-image-flatten image))))			'\
' (gimp-file-save RUN-NONINTERACTIVE image layer "$@" "$@"))		'\
'(gimp-quit 0)'

pub:
	rm -rf pub
	mkdir pub
	cp -R -t pub index.html s5-blank $(wildcard *.svg *.png)

.PHONY: all clean pub
